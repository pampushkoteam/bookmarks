package com.pampushko.bookmarks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

/**
 * Загрузка тестовых данных (закладок) в приложение
 */
@Component
public class DemodataLoader implements CommandLineRunner
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private final BookmarksRepository bookmarksRep;
	private final TagsRepository tagsRep;
	
	@Autowired
	public DemodataLoader(BookmarksRepository bookmarksRepository, TagsRepository tagsRepository)
	{
		this.bookmarksRep = bookmarksRepository;
		this.tagsRep = tagsRepository;
	}
	
	@Override
	public void run(String... strings) throws Exception
	{
		addBookmarksAndTags();
	}
	
	private void addBookmarksAndTags()
	{
		Tags java = this.tagsRep.save(new Tags("java"));
		Tags english = this.tagsRep.save(new Tags("english"));
		
		Bookmarks bookmark1 = new Bookmarks("первая закладка", "это очень интересная закладка", "http://google.ru");
		this.bookmarksRep.save(bookmark1);
		bookmark1.addTag(java);
		bookmark1.addTag(english);
		this.bookmarksRep.save(bookmark1);
		
		Bookmarks bookmark2 = new Bookmarks("вторая закладка", "это очень интересная закладка, не хуже первой", "http://yahoo.com");
		this.bookmarksRep.save(bookmark2);
		bookmark2.addTag(java);
		this.bookmarksRep.save(bookmark2);
	}
}
