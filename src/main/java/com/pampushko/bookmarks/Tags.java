package com.pampushko.bookmarks;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 */
@Data
@Entity
@Table(name = "tags")
public class Tags
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	@Column(nullable = false)
	private String name;
	
	@ManyToMany(mappedBy = "tags")
	private List<Bookmarks> bookmarks = new ArrayList<>();
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Tags tag = (Tags) o;
		return Objects.equals(name, tag.name);
	}
	
	@Override
	public int hashCode()
	{
		return Objects.hash(name);
	}
	
	public Tags()
	{
	
	}
	
	public Tags(String name)
	{
		this.name = name;
	}
	
	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("Tags{")
				.append("id=")
				.append(id)
				.append(", name='")
				.append(name)
				.append('\'')
				.append('}');
		return builder.toString();
	}
}
