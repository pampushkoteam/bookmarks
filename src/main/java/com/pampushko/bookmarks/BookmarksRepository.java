package com.pampushko.bookmarks;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 */
@RepositoryRestResource(path = "bookmarks", collectionResourceRel = "bookmarks", itemResourceDescription = @Description("закладка"), collectionResourceDescription = @Description("коллекция закладок"))
public interface BookmarksRepository extends CrudRepository<Bookmarks, Long>
{

}
