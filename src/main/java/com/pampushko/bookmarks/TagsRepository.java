package com.pampushko.bookmarks;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 */
@RepositoryRestResource(
		path = "tags",
		collectionResourceRel = "tags",
		itemResourceRel = "tag",
		itemResourceDescription = @Description("тег"),
		collectionResourceDescription = @Description("коллекция тегов")
)
public interface TagsRepository extends CrudRepository<Tags, Long>
{

}
