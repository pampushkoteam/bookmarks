package com.pampushko.bookmarks;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;

/**
 * Этот класс будет хранить закладки добавляемые пользователем
 * todo сделать возможность хранить закладки с указанием юзера-автора и публично/приватно
 */
@Data
@Entity
@Table(name = "bookmarks")
public class Bookmarks
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private @Id
	@GeneratedValue
	Long id;
	private String name;
	private String description;
	private String url;
	
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "bookmarks_tags",
			joinColumns = @JoinColumn(name = "bookmarks_id"),
			inverseJoinColumns = @JoinColumn(name = "tags_id"))
	private List<Tags> tags = new ArrayList<>();
	
	public void addTag(Tags tag)
	{
		tags.add(tag);
		tag.getBookmarks().add(this);
	}
	
	public void removeTag(Tags tag)
	{
		tags.remove(tag);
		tag.getBookmarks().remove(this);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (!(o instanceof Bookmarks)) return false;
		return id != null && id.equals(((Bookmarks) o).id);
	}
	
	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() :0;
		result = 31 * result + (name != null ? name.hashCode() :0);
		result = 31 * result + (description != null ? description.hashCode() :0);
		result = 31 * result + (url != null ? url.hashCode() :0);
		result = 31 * result + (tags != null ? tags.hashCode() :0);
		return result;
	}
	
	public Bookmarks()
	{
	}
	
	public Bookmarks(String name, String description, String url)
	{
		this.name = name;
		this.description = description;
		this.url = url;
	}
	
	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		builder.append("Bookmarks{")
				.append("id=")
				.append(id)
				.append(", name='")
				.append(name)
				.append('\'')
				.append(", description='")
				.append(description)
				.append('\'')
				.append(", url='")
				.append(url)
				.append('\'')
				.append(", tags=")
				.append(tags.toString())
				.append('}');
		return builder.toString();
	}
}
