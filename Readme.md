## Описание проекта
 
Сервис для сохранения закладок из браузера
 
## Мотивация
 
Часто приходится где-то хранить закладки, а хорошего сервиса нет. Был delicious, но он прекратил существование.
 
## Зависимости
### http://start.spring.io
* Rest Repositories
* Thymeleaf
* JPA
* H2
* Lombok
 
## Contributors
 
Alexander Pampushko
 
## License
 
BSD license
